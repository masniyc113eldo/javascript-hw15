const result = document.querySelector('#result');

document.querySelector('.btn-click').addEventListener('click', () => {
    setTimeout(() => {
        result.innerText = "Операція виконана успішно!";
    }, 3000);
});

const countdownElement = document.getElementById('countdown');
let counter = 9;

const countdownInterval = setInterval(() => {
    countdownElement.innerText = 'Зворотній відлік: ' + counter;
    counter--;

    if (counter < 0) {
        countdownElement.innerText = 'Зворотній відлік завершено';
        clearInterval(countdownInterval);
    }
}, 1000); 
